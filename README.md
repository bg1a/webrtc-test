Video Streaming by Means of WebRTC Technology  
=============================================  


Install  
-------
NodeJS:  
```
sudo apt-get install node  
sudo apt-get install npm  
```
NodeJS packages:  
```
npm install -g node-static  
npm install -g debug  
npm install -g socket.io  
```
You might need to update the NodeJS in case of errors in the Socket.IO.  
```
sudo npm cache clean -f  
sudo npm install -g n  
sudo n stable  
```


Usage
-----

Run NodeJS server:  
```
node server.js  
or  
nodejs server.js  
```
Enable server debug logging:  
```
DEBUG=server nodejs server.js
```

Run Video source:  
connect by Web browser to the Server and load source.html  
```
http://<NodeJSServerIPAddress>:8080/source.html  
```

Run Video sink:  
connect by Web browser to the Server and load sink.html:  
```
http://<NodeJSServerIPAddress>:8080/sink.html  
```

